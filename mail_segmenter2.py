import pathlib

import extract_msg
from email import policy
from email.parser import BytesParser
import pandas as pd
import re
from enum import Enum
from email.iterators import typed_subpart_iterator
from glob import glob
from os.path import basename
# from polyglot.text import Text, Word
from pathlib import Path

"""
Take a directory of email messages and return a dataframe,
with a row for every atomic message (corresponding to a specific From: header)
"""

import pdfplumber

def get_text_from_pdf(path):
    # The function gets a pdf file path
    # The function returns all free text
    with pdfplumber.open(path) as pdf2:
        pdf_text = ''
        for page in pdf2.pages:
            text = page.extract_text()
            if text!=None:
                pdf_text += text

    return pdf_text

class State(Enum):
    INIT = 1
    HEAD = 2
    BODY = 3
    TAIL = 4


class MessageParser:
    def __init__(self):
        self.state = State.BODY
        self.from_pat = re.compile("From:(.+)$", re.MULTILINE)
        self.to_pat = re.compile("To:(.+)$", re.MULTILINE)
        self.cc_pat = re.compile("Cc:(.+)$", re.MULTILINE)
        self.subject_pat = re.compile("Subject:(.+)$", re.MULTILINE)
        self.sent_pat = re.compile("Sent:(.+)$", re.MULTILINE)
        # self.email_addr_pat=re.compile("<([^@]+@.?+)>")
        self.regards_pat = re.compile("Regards")
        self.tel_pat = re.compile("Tel:")
        self.cur_record = {}
        self.record_lst = []
        self.cur_body_lst = []
        self.msg_record_map = {"sender": "from",
                               "to": "to",
                               "date": "date",
                               "subject": "subject",
                               "body": "body"}

    def move(self, line):
        if self.state == State.BODY:
            self.cur_body_lst.append(line)

        m = re.match(self.from_pat, line)
        if m is not None:
            if self.cur_record:
                self.cur_record.clear()
            self.cur_record["from"] = m.group(1).strip()
            self.state = State.HEAD

        if self.state == State.HEAD:
            m = re.match(self.to_pat, line)
            if m is not None:
                self.cur_record["to"] = m.group(1).strip()
            # self.state=State.HEAD

            m = re.match(self.cc_pat, line)
            if m is not None:
                self.cur_record["cc"] = m.group(1).strip()
            # self.state=State.HEAD

            m = re.match(self.sent_pat, line)
            if m is not None:
                self.cur_record["date"] = m.group(1).strip()
            # self.state=State.HEAD

            m = re.match(self.subject_pat, line)
            if m is not None:
                self.cur_record["subject"] = m.group(1).strip()
                self.state = State.BODY
                self.cur_record["body"] = ""

        if self.state in [State.HEAD, State.BODY]:
            m = re.match(self.regards_pat, line)
            if m is not None:
                self.state = State.TAIL
                self.cur_record["body"] = "\n".join(self.cur_body_lst)
                self.cur_body_lst = []
                self.record_lst.append(self.cur_record)
                self.cur_record = {}

            m = re.match(self.tel_pat, line)
            if m is not None:
                self.state = State.TAIL
                self.cur_record["body"] = "\n".join(self.cur_body_lst)
                self.cur_body_lst = []
                self.record_lst.append(self.cur_record)
                self.cur_record = {}

    def get_non_null_field(self, part, msg, field_name):
        if part[field_name] is not None:
            return part[field_name]
        else:
            return msg[field_name]

    def has_submessages(self, msg_text):
        if re.search("From:", msg_text):
            return True
        else:
            return False

    def extract_messages_from_text(self, cur_text):
        for line in cur_text.split("\n"):
            self.move(line)
        if self.cur_record:
            self.cur_record["body"] = "\n".join(self.cur_body_lst)
            self.cur_body_lst = []
            self.record_lst.append(self.cur_record.copy())
            self.cur_record.clear()
        return self.record_lst

    def get_message_record_list(self, cur_text):
        cur_list = self.extract_messages_from_text(cur_text)
        return cur_list

    def get_charset(self, message, default="ascii"):
        """Get the message charset"""

        if message.get_content_charset():
            return message.get_content_charset()

        if message.get_charset():
            return message.get_charset()

        return default

    def get_msg_records(self, file_path):
        #msg = extract_msg.Message(file_path)
        cur_text = get_text_from_pdf(file_path)
        self.cur_record = {}
        record_lst = []
        # for k,v in self.msg_record_map.items():
        # cur_text=msg.__getattribute__("body")
        #cur_text = '''10/9/2020 Pilotdelivers.com Mail - FW: please quote 18202 to 11783 Saturday delivery
#From: ABE Station <abe@pilotdelivers.com>
#Subject: FW: please quote 18202 to 11783 Saturday delivery
#1 message
#Joann Yeager <Joann.yeager@dmtrans.com> Fri, Oct 9, 2020 at 10:55 AM
#PLEASE QUOTE: PICKUP TODAY BY 3:30 PM AND DELIVERY TOMORROW SATURDAY 10/10 BY 4PM
#FORBO
#HAZLETON PA 18202

#Shipping to: 2043 AZALEA COURT
#                     SEAFORD NY 11783

#1 skid @ 265 lbs. 42X42X40
#FLOORING MATERIAL
#CLASS 70

#https://mail.google.com/mail/b/ALGkd0zofLi1fG2LGZA9F1u9YNyvw3u0nXzmpsmHEojnH-nVQw4-/u/0?ik=3f275d4f1a&view=pt&search=all&permthid=… 1/1'''
        cur_text = getEmails(cur_text)
        cur_text = delete_data(cur_text)
        print(cur_text)
        self.cur_record["body"] = cur_text
        if self.has_submessages(cur_text):
            self.state = State.BODY
            record_lst.extend(self.get_message_record_list(cur_text))
        else:
            record_lst.append(self.cur_record)

        return record_lst

    def get_eml_records(self, file_path):
        record_lst = []
        self.cur_record = {}
        with open(file_path, 'rb') as fp:
            msg = BytesParser(policy=policy.default).parse(fp)
            for field_name in ["From", "To", "Subject", "Date"]:
                self.cur_record[field_name.lower()] = msg[field_name]
            self.state = State.BODY
            if msg.is_multipart():
                # get the plain text version only
                text_parts = [part
                              for part in typed_subpart_iterator(msg,
                                                                 'text',
                                                                 'plain')]
                for part in text_parts:
                    charset = self.get_charset(part, self.get_charset(msg))
                    cur_text = str(part.get_payload(decode=True),
                                   charset,
                                   "replace")
                    record_lst.extend(self.get_message_record_list(cur_text))
            else:  # if it is not multipart, the payload will be a string
                # representing the message body
                charset = self.get_charset(msg, self.get_charset(msg))
                cur_text = str(part.get_payload(decode=True),
                               charset,
                               "replace")
                record_lst.extend(self.get_message_record_list(cur_text))
        return record_lst

    def get_file_extension(self, cur_path):
        # file_extension = cur_path.split('.')[-1].lower()
        file_extension = cur_path.suffix.lower()[1:]
        return file_extension

    def parse_message(self, email_path):
        file_extension = self.get_file_extension(email_path)
        if file_extension == 'pdf':
            record_lst = self.get_msg_records(email_path)
        elif file_extension == 'msg':
            record_lst = self.get_msg_records(email_path)
        elif file_extension == 'eml':
            record_lst = self.get_eml_records(email_path)
        else:
            print('File not supported')
            record_lst = []
        res_df = pd.DataFrame.from_records(record_lst)
        base_name = basename(email_path)
        res_df.loc[:, "file"] = base_name
        return res_df

    def parse_folder(self, email_folder):
        #msg_paths = list(email_folder.glob("*.msg"))
        #eml_paths = list(email_folder.glob("*.eml"))
        msg_paths = list(email_folder.glob("*.pdf"))
        #msg_paths.extend(eml_paths)
        print(f"found {len(msg_paths)} paths")
        df_lst = [self.parse_message(msg) for msg in msg_paths]
        return pd.concat(df_lst)


# def clean_string(string):
#     string=" ".join(re.split(r"(?i)([A-Z]+)",string=string))
#     string=re.sub(r'/', ' / ', string)
#     string=re.sub('’', "'", string)
#     string=re.sub(' +', ' ', string)
#     string=re.sub('~', " ~ ", string)
#     string = re.sub('(\\n\s+?)+', "\n", string)
#     string = re.sub('(\\n\s?)+', "\n", string)
#     # string = re.sub(r'^$\n\n', '', string, flags=re.MULTILINE)
#     return string
def get_lang(s):
    text = Text(s)
    return text.language.code


def get_datetime_eng(r):
    return pd.to_datetime(r["date"])


def get_datetime_heb(r):
    return ""


def get_datetime(r):
    if r["date_lang"] == "iw":
        return get_datetime_heb(r)
    else:
        return get_datetime_eng(r)


import re

def delete_data(str):
    str = re.sub(r"[a-zA-Z0-9\.\-+]+@[a-zA-Z0-9\.\-+]+\.[a-zA-Z]+",'', str)
    str = re.sub(r"(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s/-]{0,2}?\d{3}[\s-]?\d{4}",'', str)
    rurl = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    str = re.sub(rurl,'',str)
    str = str.replace("<>", "")
    #print('emails=',remails)
    #print('contact_phone=',rcontact_phone)
    #print('rurl=',rurl)
    return str



def getEmails(str):

    regex = r'([\w0-9._-]+@[\w0-9._-]+\.[\w0-9_-]+)'
    from_place,to_place = re.search(regex, str).start(),re.search(regex, str).end()
    s = str[0:from_place]
    index = s.rfind('\n') +1
    str = str[:index] + 'From: ' + str[index:]

    s = str[to_place:to_place+8] # add 'from' and /n length
    index = s.rfind('\n') +1
    str = str[:to_place+index] + 'Subject: ' + str[to_place+index:]
    return str


def main():
    message_parser=MessageParser()
    root_path = Path(r"C:\PythonProjects\BOOKING\data/")
    out_dir=Path(r"C:\PythonProjects\BOOKING\texts/")

    #root_path = Path(r"C:\PythonExamples\mails_nlp-master\data/")
    #out_dir=Path(r"C:\PythonExamples\mails_nlp-master\texts/")

    for p in root_path.iterdir():
        res_df=message_parser.parse_folder(p)
        base=p.name
        file_name='.'.join([base,"tsv"])
        out_file=out_dir/file_name
        res_df.to_csv(out_file,
                  sep="\t", 
                  index=False,
                  encoding="utf8")
    #eml_path=''.join([email_dir,"FF_UPS   export to PIRAEUS.eml"])
   
    # print(res_df.head())
    # print(res_df.info())
    # out_path="C:/Users/fyuva/projects/biyond/data/texts/test1.tsv"
    
    # for r in records:
    #     print("=========")
    #     for k,v in r.items():
    #         print(f"{k}==>{v}")
 #   print(get_mail_body(eml_path))




if __name__ == "__main__":
    main()