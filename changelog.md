## Version 1.3.0 to 1.3.1
---
### What's New
---

### What's Deprecated
---

### What's Changed
---
`GET` /document/headers Get document headers
    Return Type

        Insert documentHeaders.fileType
		
`DocumentTypeEnum` Changed to `FileTypeEnum`

## Version 1.2.8 to 1.3.0
---
### What's New
---
* `GET` /booking/document/{documentId} Get the booking document details
* `GET` /booking/parties/{documentId} Get the booking document parties
* `GET` /booking/parties/{documentId}/{partyId} Get the booking document party details
* `GET` /booking/routes/{documentId} Get the booking document routes
* `GET` /booking/process/routes/{documentId}/{routeId} Get the booking document route details
* `GET` /booking/process/shipments/{documentId} Get the booking document shipments
* `GET` /booking/process/shipments/{documentId}/{shipmentId} Get the booking document shipment details
* `GET` /booking/process/{documentId} Get number of routes/parties/shipments for all the tab
* `PUT` /booking/process/{documentId} Update StarDox with accepted route/parties/shipments
* `GET` /invoice/document/{documentId} Get the invoice document details
* `GET` /invoice/parties/{documentId} Get the invoice document parties
* `GET` /invoice/parties/{documentId}/{partyId} Get the invoice document party details
* `GET` /invoice/details/{documentId} Get the invoice document details
* `GET` /invoice/details/{documentId}/{itemNumber} Get the invoice document item details
* `GET` /invoice/payment/{documentId} Get the invoice document payment details
* `GET` /invoice/general/{documentId} Get the invoice document general details
* `GET` /invoice/remarks/{documentId} Get the invoice document remarks details
* `GET` /invoice/remarks/{documentId}/{remarkNumber} Get the invoice document remark details
* `GET` /invoice/process/{documentId} Get number of invoice details for all the tab
* `PUT` /invoice/process/{documentId} Update StarDox with accepted invoice details
* `GET` /warehouse/document/{documentId} Get the warehouse document details
* `GET` /warehouse/parties/{documentId} Get the warehouse document parties
* `GET` /warehouse/parties/{documentId}/{partyId} Get the warehouse document party details
* `GET` /warehouse/inboundoutbound/{documentId} Get the warehouse document inbounde/outbound
* `GET` /warehouse/inboundoutbound/{documentId}/{itemNumber} Get the warehouse document in/out details
* `GET` /warehouse/commodity/{documentId} Get the warehouse document commodities
* `GET` /warehouse/commodity/{documentId}/{itemNumber} Get the warehouse document commodities details
* `GET` /warehouse/general/{documentId} Get the warehouse document general details
* `GET` /warehouse/remarks/{documentId} Get the warehouse document remarks details
* `GET` /warehouse/remarks/{documentId}/{remarkNumber} Get the warehouse document remark details
* `GET` /warehouse/process/{documentId} Get number of invoice details for all the tab
* `PUT` /warehouse/process/{documentId} Update StarDox with accepted warehouse details


### What's Deprecated
---

### What's Changed
---
* `GET` /document/headers Get document headers
  * Parameters
    
		 Add limit (Limit the results)
		 Modify `categoryFilter`
  * Return Type
    
		Insert `documentHeaders.documentActivity` 
		Delete `documentHeaders.documentCategory`

* `POST` ​/file Upload new document	
	* Parameters

		Insert `documentHeaders.documentActivity` 
		Delete `documentHeaders.documentCategory`

## Version 1.2.7 to 1.2.8
---
### What's New
---

### What's Deprecated
---

### What's Changed
---
`getChargesForRouteResponse.charges.businessEffect.amount` changed type to      
    
		String

## Version 1.2.6 to 1.2.7
---
### What's New
---

### What's Deprecated
---

### What's Changed
---
`getChargesForRouteResponse.charges.marginEffect.amount` changed type
	Changed type
	
	
		String

`getChargesForRouteResponse.charges.marginEffect.posOrNev` add values
	New
		
		NONE
		
`getChargesForRouteResponse.charges.businessEffect.posOrNev` add values
	New
		
		NONE


## Version 1.2.5 to 1.2.6
---
### What's New
---

### What's Deprecated
---

### What's Changed
---
`GET` /document/headers Get document headers
    Parameters

        Modify categoryFilter

`documentTypeEnum` values
	New
	
		HISTORY

`documentCategory` values
	New
	
		BUYING_HISTORY, SELLING_HISTORY
		

## Version 1.2.4 to 1.2.5
---
### What's New
---

### What's Deprecated
---

### What's Changed
---
`GET` /document/headers Get document headers
    Parameters

        Modify categoryFilter
    Return Type

        Insert documentHeaders.proposalId //The proposal ID. Only relevant to purchase orders
		
`documentCategory` values
	New
		
		PURCHASE_ORDER